# Python pacmantools
Command line tools and a python library to read from Arch Linux's pacman database of locally installed packages and scratch some everyday itches.

## Getting started

### Prerequisites
* Be on an Arch linux system :-)
* Use python 3

### Installing
* Clone this repo
* Run python setup.py install

## Usage
Pacmantools provides two CLI tools today, pacls and pacunwanted:

### Pacls
* Lists installed packages, either by name or by size.
* Gives either fixed width output for readability in a terminal or comma separated to use spreadsheet-wise.
* Use --help to get help.

```
$ pacls --help
usage: -c [--help] [-l] [--csv] [-v] [-S] [-h] [pkgs]

Query the pacman database of installed packages. Print like 'ls' does for
files.

positional arguments:
  pkgs                  Package names to filter on. Accepts globbing syntax
                        for pkg names and versions as found in
                        /var/lib/pacman/local.

optional arguments:
  --help                show this help message and exit
  -l, --long            Use a long listing format
  --csv                 Use the long listing format comma separated with a
                        header.
  -v, --verbose         Increase verbosity. Default = warnings, -v = info, -vv
                        = debug.
  -S                    Sort packages by size
  -h, --human-readable  print sizes like 1K 234M 2G etc
```


### Pacunwanted
* Allows arch sysadmins to maintain a list of packages to keep in ~/.config/pacmantools.ini.
* Lists unwanted packages: unrequired installed packages not in the list of
groups and packages to keep.

```
$ pacunwanted --help
usage: pacunwanted [--help] [-l] [--csv] [-v] [-S] [-h]

List unwanted packages: unrequired installed packages not in the list of
groups and packages to keep.

optional arguments:
  --help                show this help message and exit
  -l, --long            Use a long listing format
  --csv                 Use the long listing format comma separated with a
                        header.
  -v, --verbose         Increase verbosity. Default = warnings, -v = info, -vv
                        = debug.
  -S                    Sort packages by size, default by name.
  -h, --human-readable  print sizes like 1K 234M 2G etc

Allowed unrequired packages, i.e. packages to keep, must be listed in
~/.config/pacmantools.ini in a 'keep' section. Pacunwanted will recommend to
remove all packages not listed in this file except those in the base group,
which will never be proposed for removal. [keep] packages=less vim git python
groups=base base-devel Refer to
https://docs.python.org/3/library/configparser.html#quick-start for more info
on config file syntax.
```

## State
Pretty alpha. But I use it :-)

## Plans
Some kind of dependency info. We want to chase orphans! Keep things clean! Throw bloated stuff that takes too much space out.

## Issues, questions, requests, suggestions
Gitlab provides an [issue tracker](https://gitlab.com/plenae/python-pacmantools/issues) ;-)
