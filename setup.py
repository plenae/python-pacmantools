from setuptools import setup

with open('README.md') as f:
    long_description = f.read()

setup(
    name="pacmantools",
    version="0.0.2",
    author="Pieter Lenaerts",
    author_email="pieter dot aj dot lenaerts at gmail",
    description=("Command line tools and a library to read from Arch "
                 "Linux's pacman database of locally installed packages "
                 "and scratch some everyday itches."),
    license="GPL",
    keywords="arch linux pacman package management",
    url="https://gitlab.com/plenae/python-pacmantools",
    packages=['pacmantools'],
    long_description=long_description,
    long_description_content_type='text/markdown',
    long_description_markdown_filename='README.md',
    install_requires=['globre'],
    setup_requires=['setuptools-markdown'],
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Topic :: Utilities",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
    ],
    entry_points={
        'console_scripts': [
            'pacls=pacmantools.pacls:main',
            'pacunwanted=pacmantools.pacunwanted:main'
        ]
    }
)
