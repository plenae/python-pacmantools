#!/usr/bin/env python

import argparse
import logging
import subprocess
import shlex
from pacmantools import pacls
from pacmantools import paclib
from pacmantools import util


def __get_parser__():
    """
    Return the ArgumentParser for pacunwanted.

    :returns: ArgumentParser
    """
    # TODO: work with a parent parser. pacunwanted and pacls share a lot of
    # arguments
    parser = argparse.ArgumentParser(
        description=('List unwanted packages: unrequired installed packages '
                     'not in the list of groups and packages to keep.'),
        epilog='''Allowed unrequired packages, i.e. packages to keep, must be
        listed in ~/.config/pacmantools.ini in a 'keep' section.\n
        Pacunwanted will recommend to remove all packages not listed in
        this file except those in the base group, which will never be proposed
        for removal.\n\n
        [keep]\n
        packages=less vim git python\n
        groups=base base-devel\n\n
        Refer to
        https://docs.python.org/3/library/configparser.html#quick-start
        for more info on config file syntax.
        ''',
        add_help=False)
    parser.add_argument('--help', action='help', default=argparse.SUPPRESS,
                        help=argparse._('show this help message and exit'))
    parser.add_argument('-l', '--long', action='store_true',
                        dest='out_type_long', help='Use a long listing format')
    parser.add_argument('--csv', action='store_true', dest='out_type_csv',
                        help='Use the long listing format comma separated '
                        'with a header.')
    parser.add_argument('-v', '--verbose', action='count', default=0,
                        help='Increase verbosity. Default = warnings, '
                        '-v = info, -vv = debug.')
    parser.add_argument('-S', dest='sort_size', action='store_true',
                        help='Sort packages by size, default by name.')
    parser.add_argument('-h', '--human-readable', action='store_true',
                        dest='size_human',
                        help='print sizes like 1K 234M 2G etc')
    return parser


def get_pkgs_in_group(group):
    """
    Return a set of packages part of group *group.

    Based on the output of pacman -Qg.

    :param group: The package group to list the member packages for.
    :returns: Set of package names as strings, part of the group.
    """
    pkgs_in_group = set()
    try:
        proc_out = subprocess.check_output(['pacman', '-Qg', group],
                                           universal_newlines=True)
        pkgs_in_group = {pkg.split(' ')[1] for pkg in
                         proc_out.split('\n')[:-1]}
    except subprocess.CalledProcessError:
        logging.warning('Group ' + group + ' in config does not exist.')

    # TODO: write unit tests for this!
    # Pacman -Qg output ends with a newline, so we drop the last
    # element in the split('\n').
    # Next, each package is in the second element of the split(' ')
    logging.debug('Packages in group ' + group + ': ' + str(pkgs_in_group))
    return pkgs_in_group


def get_unrequired_pkgs():
    """
    Return a set of unrequired packages.

    Refer to pacman -Qt documentation: unrequired packages are neither
    required nor optionally required by any currently installed package.

    :returns: Set of unrequired package names.
    """
    proc_out = subprocess.check_output(['pacman', '-Qt'],
                                       universal_newlines=True)
    return {pkg.split(' ')[0] for pkg in proc_out.split('\n')[:-1]}


def get_unwanted(config):
    """
    Return a set of packages up for removal: which are unrequired and not in
    the keep lists.

    :param config: configparser parsed from ~/.config/pacmantools.ini
    :returns: set of packages names which are up for removal.
    """
    allowed_groups = {'base'}
    keep = get_pkgs_in_group(allowed_groups.pop())

    try:
        # TODO: split this up in different try catches. if we don't have a
        # packages key we don't get to reading the groups...
        keep.update({p for p in shlex.split(config['keep']['packages'])})
        logging.debug('Pkgs to keep in config: ' + str(keep))
    except KeyError as e:
        if str(e) == '\'keep\'':
            logging.warning(('Config file does not contain a \'keep\' '
                             'section. Pacunwanted will list all unrequired '
                             'packages not in the base group!'
                             ))
        elif str(e) == '\'packages\'':
            logging.warning(('Config file does not contain a \'packages\' key '
                             'in the \'keep\' section. Pacunwanted will list '
                             'all unrequired packages not in the \'base\' '
                             'group and not in the groups in the \'groups\' '
                             'key, if that key exists in the \'keep\' section.'
                             ))
        else:
            raise(e)
    except Exception as e:
        raise(e)

    try:
        allowed_groups.update({p for p in
                               shlex.split(config['keep']['groups'])})
        logging.debug('Base group and groups to keep in config: ' +
                      str(allowed_groups))
    except KeyError as e:
        if str(e) == '\'groups\'':
            logging.warning(('Config file does not contain a \'groups\' key '
                             'in the \'keep\' section. Pacunwanted wil list '
                             'all unrequired packages not in the \'base\' '
                             'group and not in the \'packages\' key if that '
                             'key exists.'
                             ))
        else:
            raise(e)
    except Exception as e:
        raise(e)

    for group in allowed_groups:
        keep.update(get_pkgs_in_group(group))
    logging.debug('Pkgs to keep from packages and groups: ' +
                  str(keep))

    unreq = get_unrequired_pkgs()
    logging.debug('Unrequired packages installed: ' + str(unreq))

    unwanted = unreq - keep
    return unwanted


def main():
    """Run pacunwanted."""
    args = __get_parser__().parse_args()
    util.config_logging(args.verbose)
    logging.debug('Args: ' + str(args))

    out_type = pacls.get_out_type(args.out_type_long, args.out_type_csv)
    logging.debug(out_type)

    sort = pacls.get_sort(args.sort_size)
    logging.debug(sort)

    size = pacls.get_size_out(args.size_human)
    logging.debug(size)

    config = paclib.get_config()
    unwanted_names = get_unwanted(config)
    logging.debug('Unwanted: ' + str(unwanted_names))

    if not (args.out_type_long or args.sort_size):
        unwanted_names = list(unwanted_names)
        unwanted_names.sort()
        print('\n'.join(unwanted_names))
    else:
        unwanted_pkgs = set()
        for pkg_name in unwanted_names:
            pkg_dirs = paclib.get_pkg_dirs(pkg_name)
            logging.debug('Globbed for pkg name ' + pkg_name +
                          ': ' + str(pkg_dirs))
            for p in pkg_dirs:
                unwanted_pkgs.add(paclib.package(p))
        unwanted_pkgs = sort(unwanted_pkgs)
        for p in unwanted_pkgs:
            out_type(p, size)


if __name__ == '__main__':
        main()
