import logging


def config_logging(level):
    """Configure a basic logger at loglevel *level*"""
    if level == 0:
        log_level = logging.WARNING
    elif level == 1:
        log_level = logging.INFO
    elif level > 1:
        log_level = logging.DEBUG

    logging.basicConfig(level=log_level)
    logging.info('Log level is ' + logging.getLevelName(log_level))
