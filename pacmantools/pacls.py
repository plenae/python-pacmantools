"""CLI module to query the pacman database of installed packages."""

import argparse
import logging
from pacmantools import paclib
import collections
import shlex


def print_short(p, size_out):
    """
    Print the name of the package p.

    :param p: pacmantools.paclib.package to print the name of.
    :param size_out: not used here. Present to respect the interface of the
    other print functions.
    """
    # TODO: print to | head breaks. see https://stackoverflow.com/questions/15793886/how-to-avoid-a-broken-pipe-error-when-printing-a-large-amount-of-formatted-data
    print(p.name)


def print_long_csv(p, size_out):
    # TODO: this is too basic. We should use the csv module to avoid all kinds
    # of bad things.
    """
    Print the long listing format for package p.

    :param p: pacmantools.paclib.package to print the name of.
    :param size_out: function to get the package.size in the wanted form.
    """
    logging.debug('Package object: ' + str(p))
    ordered = collections.OrderedDict([
        ('name', p.name),
        ('version', p.version),
        ('size', size_out(p.size)),
        ('desc', p.desc)
    ])
    # todo: see above, printing to head breaks. Annoyingly.
    print(','.join([str(v) for v in ordered.values()]))


def print_long_fixed(p, size_out):
    """
    Print package info in fixed width.

    :param p: pacmantools.paclib.package to print the name of.
    :param size_out: function to get the package.size in the wanted form.
    """
    logging.debug('Package object: ' + str(p))
    print('{0.name:20} {0.version:20} '
          '{1:10} {0.desc}'.format(p, size_out(p.size)))


def ordered_size(pkgs):
    """Return an OrderedDict of packages sorted on size.

    :param pkgs: a collection of pacmantools.paclib.package objects to sort.
    :returns: an Orderedict with keys = package objects, values = sizes,
    sorted on sizes.
    """
    d = {p: p.size for p in pkgs}
    return reversed(collections.OrderedDict(sorted(d.items(),
                                                   key=lambda t: t[1])))


def ordered_name(pkgs):
    """
    Return an OrderedDict sorted on names.

    :param pkgs: a collection of pacmantools.paclib.package objects to sort.
    :returns: an Orderedict with keys = package objects, values = names,
    sorted on names.
    """
    d = {p: p.name for p in pkgs}
    return collections.OrderedDict(sorted(d.items(), key=lambda t: t[1]))


def size_human(size):
    """Convert a size int in bytes to a float + suffix in steps of 1024.

    :param size: int in bytes to convert
    :returns: a string float rounded to 2 suffixed with the correct unit
    suffix.
    """
    suf = 'B'
    power = 0
    if size // 1024**3 > 0:
        suf = 'G'
        power = 2
    elif size // 1024**2 > 0:
        suf = 'M'
        power = 2
    elif size // 1024 > 0:
        suf = 'K'
        power = 1
    size = str(round(size / 1024**power, 2))
    return size + suf


def size_bytes(size):
    """
    Return the size int without a bytes suffix (B).

    :param size: an int representing bytes.
    :returns: str(size)
    """
    # TODO: this function does nothing. Thats lame.
    # Not adding B. The B gets in the way when processing the results.
    return str(size)


def __get_parser__():
    """Return the ArgumentParser for pacls."""
    parser = argparse.ArgumentParser(
        description='Query the pacman database of installed packages. '
        'Print like \'ls\' does for files.', add_help=False)
    # Found this recipe on https://www.reddit.com/r/learnpython/comments/3gbkin/overriding_h_short_option_in_argparse/
    # Override default help argument so that only --help (and not -h)
    # can call help
    parser.add_argument('--help', action='help', default=argparse.SUPPRESS,
                        help=argparse._('show this help message and exit'))
    parser.add_argument('packages', metavar='pkgs', nargs='?', default='*',
                        help=('Package names to filter on. '
                              'Accepts globbing syntax for pkg names and '
                              'versions as found in /var/lib/pacman/local.'))
    parser.add_argument('-l', '--long', action='store_true', dest='out_type',
                        help='Use a long listing format')
    parser.add_argument('--csv', action='store_true', dest='out_type_csv',
                        help='Use the long listing format comma separated '
                        'with a header.')
    parser.add_argument('-v', '--verbose', action='count', default=0,
                        help='Increase verbosity. Default = warnings, '
                        '-v = info, -vv = debug.')
    parser.add_argument('-S', dest='sort_size', action='store_true',
                        help='Sort packages by size')
    parser.add_argument('-h', '--human-readable', action='store_true',
                        dest='size_human',
                        help='print sizes like 1K 234M 2G etc')
    return parser


def get_out_type(out_long, out_csv):
    if out_long:
        out_type = print_long_fixed
        if out_csv:
            out_type = print_long_csv
            # TODO: find a smart way to make this follow the
            # fields in print_long_csv()
            print(','.join(['Name', 'Version', 'Size', 'Desc']))
    else:
        out_type = print_short
    return out_type


def get_sort(size):
    if size:
        sort = ordered_size
    else:
        sort = ordered_name
    return sort


def get_size_out(human):
    if human:
        size_out = size_human
    else:
        size_out = size_bytes
    return size_out


def main():
    args = __get_parser__().parse_args()

    if args.verbose == 0:
        log_level = logging.WARNING
    elif args.verbose == 1:
        log_level = logging.INFO
    elif args.verbose > 1:
        log_level = logging.DEBUG

    logging.basicConfig(level=log_level)
    logging.info('Log level is ' + logging.getLevelName(log_level))
    logging.debug('Args: ' + str(args))

    # TODO: remove in favor of get_out_type(long, csv)
    if args.out_type:
        args.out_type = print_long_fixed
        if args.out_type_csv:
            args.out_type = print_long_csv
            # TODO: find a smart way to make this follow the
            # fields in print_long_csv()
            print(','.join(['Name', 'Version', 'Size', 'Desc']))
    else:
        args.out_type = print_short

    # TODO: remove in favor of get_sort(size)
    if args.sort_size:
        sort = ordered_size
    else:
        sort = ordered_name

    # TODO: remove in favor of get_size_out(human)
    if args.size_human:
        args.size_out = size_human
    else:
        args.size_out = size_bytes

    pkg_dirs = set()
    for arg in shlex.split(args.packages):
        pkg_dirs.update(paclib.get_pkg_dirs(arg))
    logging.debug('Pkg paths: ' + str(pkg_dirs))

    # TODO: don't instantiate package objects when not sorting or longlisting
    pkgs = [paclib.package(p) for p in pkg_dirs]

    pkgs = sort(pkgs)

    for p in pkgs:
        # TODO: don't use args.out above but variables out_type and sort
        # pointing to the right functions.
        args.out_type(p, args.size_out)
