"""Module to process /var/lib/pacman"""

import os
import os.path
import configparser
import pathlib
import glob
import globre
import logging


def get_pkg_dirs(pkg_name):
    """
    Return a set of package dirs for package names matching the pkg_name
    pattern.

    :param pkg_name: glob-style pattern for package names.
    :returns: Set of package dirs in string format
    """
    match_dirs = glob.glob(os.path.join('/var/lib/pacman/local', pkg_name) +
                           '-*-*')
    pkg_dirs = set()
    for d in match_dirs:
        pkg_name_from_dir = os.path.basename('-'.join(d.split('-')[:-2]))
        if globre.match(pkg_name, pkg_name_from_dir):
            pkg_dirs.add(d)
        else:
            logging.debug('Globre: ' + pkg_name_from_dir +
                          ' does not match pattern ' + pkg_name)
    return pkg_dirs


def get_pkg_desc(pacdir_or_name):
    """
    Return an open file handler to a package's desc file in
    /var/lib/pacman/local/*

    :param pacdir_or_name: string representing either the path of a package's
    dir in /var/lib/pacman/local/ or a package name without globbing.
    :return: open file handler to a package's desc file in
    /var/lib/pacman/local/*
    """
    if not os.path.isdir(pacdir_or_name):
        pacdir_or_name = get_pkg_dirs(pacdir_or_name)
    return open(os.path.join(pacdir_or_name, 'desc'), 'r')


class package():
    def __init__(self, pacdir_or_name):
        desc = get_pkg_desc(pacdir_or_name)
        logging.debug('Desc file: ' + desc.name)
        prev = None
        for line in desc.readlines():
            line = line.strip()
            if prev == '%NAME%':
                self.name = line
            elif prev == '%VERSION%':
                self.version = line
            elif prev == '%SIZE%':
                self.size = int(line)
            elif prev == '%DESC%':
                self.desc = line
            prev = line.strip()

    def __str__(self):
        return str(self.__dict__)


def get_config():
    """Return a configparser for $HOME/.config/pacmantools.ini"""
    cp = configparser.ConfigParser()
    cp.read(os.path.join(pathlib.Path.home(), '.config', 'pacmantools.ini'))
    return cp


def get_all_installed_package_names():
    """Return a set of the names of all installed packages."""
    # good tests are '/var/lib/pacman/local/qt5-webkit-5.212.0alpha2-18',
    # '/var/lib/pacman/local/xorg-fonts-75dpi-1.0.3-3' and 'zsh-5.5.1-2'
    pkg_dirs = get_pkg_dirs('*')
    logging.debug('Got installed pkg dirs: ' + str(pkg_dirs))
    pkg_names = set()
    for pkg_dir in pkg_dirs:
        pkg_name = get_pkg_name(pkg_dir)
        pkg_names.add(pkg_name)
    return pkg_names


def get_pkg_name(pkg_dir):
    pkg_dir_base = os.path.basename(pkg_dir)
    if pkg_dir_base == '':
        pkg_dir_base = os.path.basename(os.path.dirname(pkg_dir))
    split_dir = pkg_dir_base.split('-')
    dashes = len(split_dir)
    pkg_name = '-'.join(split_dir[0:dashes - 2])
    logging.debug('Package ' + pkg_name + ' is in ' + pkg_dir)
    return pkg_name
